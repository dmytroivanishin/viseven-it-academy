"use strict"

function getName(name) {
  return "Hello, " + name;
}

const alex = "Hello, Alex";
const john = getName("John");
const merry = getName("Merry");

alert(alex);
alert(john);
alert(merry);

const getUser = name => name.toUpperCase();

alert( getUser("John") );


const getArrayNumberTo2 = (...numbers) => {
  const result = [];

  for(let i = 0; i < numbers.length; i++){
    result.push(numbers[i] * 2);
  }

  return result;
};

console.log( getArrayNumberTo2(1, 2, 3, 4, 5, 6, 7, 8, 9, 10) );

const getArrayNumberTo3 = (...numbers) => {
  const result = [];

  for(let i = 0; i < numbers.length; i++){
    result.push(numbers[i] * 3);
  }

  return result;
};

console.log( getArrayNumberTo3(1, 2, 3, 4, 5, 6, 7, 8, 9, 10) );


const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

const getNumbers = (numbers, callback) => {
  const result = [];

  for(let i = 0; i < numbers.length; i++){
    result.push( callback( numbers[i] ) );
  }

  return result;
};

const resultNumbersTo2 = getNumbers(numbers, (n) => n * 2);
console.log(resultNumbersTo2);

const resultNumbersTo3 = getNumbers(numbers, (n) => n * 3);
console.log(resultNumbersTo3);

const resultNumbersTo4 = getNumbers(numbers, (n) => n * 4);
console.log(resultNumbersTo4);

const resultNumbersTo5 = getNumbers(numbers, (n) => n * 5);
console.log(resultNumbersTo5);

const resultNumbersToSin = getNumbers(numbers, (n) => Math.sin(n));
console.log(resultNumbersToSin);


const makeCounter = () => {
  let count = 0;

  const counter = () => count++;
  const reset = () => count = 0;

  return {
    counter: counter,
    reset: reset,
  };
};

// The difference between pre/post increment
// let count = 0;
//
// console.log(count++)
// console.log(count)
//
// console.log(++count)
// console.log(count)

const interfaceCounter = makeCounter();

console.log( interfaceCounter.counter() );
console.log( interfaceCounter.counter() );
console.log( interfaceCounter.counter() );
console.log( interfaceCounter.reset() );

console.log( interfaceCounter.counter() );
console.log( interfaceCounter.counter() );
console.log( interfaceCounter.counter() );