const generateUser = (name, surname, age) => {
  return {
    name,
    surname,
    age,
    getFullname() {
      return this.name + " " + this.surname;
    }
  };
};

const users = [
  generateUser("Alex", "Smith", 30),
  generateUser("John", "Merlo", 25),
  generateUser("Alex", "SDfsdf", 64),
  generateUser("Merry", "WEfwe", 28),
  generateUser("Alex", "SDfsdf", 23),
];

const json = JSON.stringify(users)

console.log(users)
console.log(json)
console.log(JSON.parse(json));


const user = generateUser("Alex", "Smith", 30);

console.log(user)
console.log( JSON.stringify(user) )
console.log( JSON.parse( JSON.stringify(user) ) );

const add = (a, b) => a + b;

const result = add.bind(add, 1);

console.log( add.bind(add, 1)(2) );