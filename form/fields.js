const form = document.getElementById("form");

const inputs = document.querySelectorAll("input, textarea");
const checkboxes = document.querySelectorAll("input[name='checkbox-example']");

const checkboxValues = Array.from(checkboxes)
  .filter(checkbox => checkbox.checked)  
  .map(checkbox => checkbox.value);

form.addEventListener("change", (e) => {
  console.log("Event Change", e.target.value)
});

form.addEventListener("input", (e) => {
  console.log("Event Input", e.target.value)
});

form.addEventListener("submit", (e) => {
  e.preventDefault();

  console.log("Event Submit", e.target)
});

inputs.forEach(input => {
  input.addEventListener("focus", (e) => {
    console.log("Event Focus", e.target.value);
  });

  input.addEventListener("blur", (e) => {
    console.log("Event Blur", e.target.value);
  });
});