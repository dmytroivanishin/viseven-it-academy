// Match
const str = "I love JavAscriPt sf sdf sdsd sdf JavAscriPt ";

console.log( str.match(/javascript/i) );
console.log( str.match(/javascript/ig) );

// Test

// Range
console.log("Range", /[0-5]+/.test(3) );
console.log("Range", /[0-5]+/.test(6) );

// Excluding Range
console.log("Excluding Range", /[0-5]+/.test(3) );
console.log("Excluding Range", /[0-5]+/.test(6) );

// Validation for numbers using Anchors ^ and $
console.log("Validation numbers", /^[0-9]+$/.test(54645) );
console.log("Validation numbers", /^[0-9]+$/.test("533645") );
console.log("Validation numbers", /^[0-9]+$/.test("546dsfds45") );

// Replace
const data = {
  name: "John",
  text: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Eos iste tenetur consectetur consequuntur corporis magnam itaque nihil, aut similique. Accusantium!"
};

const html = `
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>RegExp</title>
</head>

<body>
  <h1>{{ name }}</h1>
  <p>{{ text }}</p>

  <script src="./script.js"></script>
</body>

</html>
`;

const result = html.replace(/{{ (\w+) }}/g, (_, key) => data[key]);

console.log(result);