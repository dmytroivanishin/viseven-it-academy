const generateUser = (name, surname, age) => {
  return {
    name,
    surname,
    age,
    getFullname() {
      return this.name + " " + this.surname;
    }
  };
};

const users = [
  generateUser("Alex", "Smith", 30),
  generateUser("John", "Merlo", 25),
  generateUser("Alex", "SDfsdf", 64),
  generateUser("Merry", "WEfwe", 28),
  generateUser("Alex", "SDfsdf", 23),
];

console.log(users);

for(let i = 0; i < users.length; i++){
  console.log( users[i].getFullname() )
}

const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

numbers.forEach(number => {
  console.log(number)
});

/*

const resultTo2 = numbers.map(number => number * 2);
console.log(resultTo2);

const resultTo3 = numbers.map(number => number * 3);
console.log(resultTo3);

const resultUsers = users.map(user => user.name.toUpperCase());
console.log(resultUsers);

const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

const filtered = numbers.filter((number) => number === 5 );
console.log(filtered);

const found = numbers.find((number) => number === 5 );
console.log(found);

console.log( users.filter(user => user.name === "Alex") )
console.log( numbers.indexOf(5) )
console.log( numbers.lastIndexOf(5) )

console.log( numbers[9] )

*/

const map = new Map();

const key = {
  id: 285425,
  toString() {
    return `{id: 285425}`;
  }
};

const key2 = {
  id: 864864,
  toString() {
    return `{id: 864864}`;
  }
};

map.set(5, "Alex");
map.set(846, "John");
map.set(key, "John");

console.log( map.get(key) );

const mapObject = {};

mapObject[key] = "Alex";
mapObject[key2] = "John";

console.log(mapObject);

alert(key)
alert(key2);


const set = new Set();

set.add(2);
set.add(4);
set.add(5);
set.add(5);
set.add(2);

for(let num of set){
  console.log(num)
}





const json = JSON.stringify(users)

console.log(users)
