# Function
## Useful links
[Function Basic](https://learn.javascript.ru/function-basics)

[Function Expression](https://learn.javascript.ru/function-expressions)

[Arrow Function](https://learn.javascript.ru/arrow-functions-basics)

[new Function](https://learn.javascript.ru/new-function)

[Recursion](https://learn.javascript.ru/recursion)

[Rest](https://learn.javascript.ru/rest-parameters-spread-operator)

[Closure](https://learn.javascript.ru/closure)

[Patterns Basic](https://gist.github.com/GoodDayTodayOkey/d266f725982d42be548ef68d3764b7d2)

[Functional Programming Basic](https://tproger.ru/translations/functional-programming-concepts/)

# Form
## Useful links
[Tag Form](https://webref.ru/html/form)

[Tag Input](https://webref.ru/html/input)

[Form Elements](https://learn.javascript.ru/form-elements)

[Events Change and Input](https://learn.javascript.ru/events-change-input)

[Events Focus and Blur](https://learn.javascript.ru/focus-blur)

[Event Submit](https://learn.javascript.ru/forms-submit)

[Event Delegation](https://learn.javascript.ru/event-delegation)

[JSON](https://learn.javascript.ru/json)

[Fetch](https://learn.javascript.ru/fetch)

[FormData](https://learn.javascript.ru/formdata)

# RegExp
## Useful links
[Everything about regexp](https://learn.javascript.ru/regular-expressions)

# Object
## Useful links
[Object](https://learn.javascript.ru/object)

[Object Copy](https://learn.javascript.ru/object-copy)

[Object Methods](https://learn.javascript.ru/object-methods)

[Keys, Values, Entries](https://learn.javascript.ru/keys-values-entries)

[Destructuring Assignment](https://learn.javascript.ru/destructuring-assignment)

# Array
## Useful links
[Array](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Array)

[Array](https://learn.javascript.ru/array)

[Array Methods](https://learn.javascript.ru/array-methods)

[Map, Set](https://learn.javascript.ru/map-set)

# JSON
## Useful links
[About JSON](https://learn.javascript.ru/json)

[About JSON](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/JSON)

[About JSON](https://developer.mozilla.org/ru/docs/Learn/JavaScript/Objects/JSON)

# My youtube channel
[Link to channel](https://www.youtube.com/channel/UCf5RTtyABDxkZabVjr5g1DQ)