const fs = require("fs");

const {
  filenameTestFiles,
  readdirPromise,
  writePromise,
  mkdir,
  getTemplates,
  getFolderToTest,
  getPathToTest
} = require("./utils");

const initTest = async () => {
  const students = await readdirPromise("./students");
  const templates = await getTemplates();

  const structure = students.map(student => {
    return templates.map((t, indexTemplate) => ({
      studentFolder: student,
      filename: filenameTestFiles[indexTemplate],
      template: t.toString()
    }));
  }).flat();

  await Promise.all(students.map(folder => {
    const destination = getFolderToTest(folder);
    if(!fs.existsSync(destination)){
      return mkdir(destination);
    }

    return "";
  })); 

  return Promise.all(structure.map(strct => writePromise(getPathToTest(strct.studentFolder, strct.filename), strct.template)));
}; 

initTest()
  .then(() => console.log("Done! Run test: npm run test | npm run test:watch"));