const { remove } = require('fs-extra');

const { readdirPromise, getFolderToTest } = require("./utils");

const removeTest = async () => {
  const students = await readdirPromise("./students");

  return Promise.all(students.map(student => remove(getFolderToTest(student))));
};

removeTest()
  .then(() => console.log("Remove folder 'test' was success!"));