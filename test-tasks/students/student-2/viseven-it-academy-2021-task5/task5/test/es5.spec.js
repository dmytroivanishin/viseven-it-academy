import { forEachMockData, makeTestEmail, makeTestUserMethod, makeTestRoleMethod } from "./utils";
import { User, Admin, Moderator, Client, Guest } from "../es5/script";

describe("ES5 classes", () => {
  const data = [
    {
      name: "John",
      surname: "Smith",
      email: "john@test.com",
      yearOfBirth: 1976
    },
    {
      name: "Marty",
      surname: "Nikolson",
      email: "marty@test.com",
      yearOfBirth: 1987
    }
  ];

  describe("User", () => {
    forEachMockData(data, User, (classInstance, currentMockData) => {
      const { name, surname, email, yearOfBirth } = currentMockData;

      describe(`User ${name} ${surname}`, () => {

        makeTestEmail(
          classInstance,
          "should be email in Instance",
          email
        );
      
        makeTestUserMethod(
          classInstance,
          "should be method getFullname in Instance and return correct data",
          "getFullname",
          `${name} ${surname}`
        );
      
        makeTestUserMethod(
          classInstance,
          "should be method getAge in Instance and return correct data",
          "getAge",
          new Date().getFullYear() - yearOfBirth
        );
      
      });
    });
  });

  describe("Admin", () => {
    forEachMockData(data, Admin, (classInstance, currentMockData) => {
      const { name, surname, email, yearOfBirth } = currentMockData;

      describe(`Admin ${name} ${surname}`, () => {

        makeTestEmail(
          classInstance,
          "should be email in Instance",
          email
        );

        makeTestUserMethod(
          classInstance,
          "should be method getFullname in Instance and return correct data",
          "getFullname",
          `${name} ${surname}`
        );

        makeTestUserMethod(
          classInstance,
          "should be method getAge in Instance and return correct data",
          "getAge",
          new Date().getFullYear() - yearOfBirth
        );

        makeTestRoleMethod(
          classInstance,
          "should be method read in Instance and return correct data",
          "read",
          `I'm ${name}. I can read.`
        );

        makeTestRoleMethod(
          classInstance,
          "should be method write in Instance and return correct data",
          "write",
          `I'm ${name}. I can write.`
        );

        makeTestRoleMethod(
          classInstance,
          "should be method update in Instance and return correct data",
          "update",
          `I'm ${name}. I can update.`
        );

        makeTestRoleMethod(
          classInstance,
          "should be method remove in Instance and return correct data",
          "remove",
          `I'm ${name}. I can remove.`
        );

      });
    });
  });

  describe("Moderator", () => {
    forEachMockData(data, Moderator, (classInstance, currentMockData) => {
      const { name, surname, email, yearOfBirth } = currentMockData;

      describe(`Moderator ${name} ${surname}`, () => {

        makeTestEmail(
          classInstance,
          "should be email in Instance",
          email
        );
        
        makeTestUserMethod(
          classInstance,
          "should be method getFullname in Instance and return correct data",
          "getFullname",
          `${name} ${surname}`
        );

        makeTestUserMethod(
          classInstance,
          "should be method getAge in Instance and return correct data",
          "getAge",
          new Date().getFullYear() - yearOfBirth
        );

        makeTestRoleMethod(
          classInstance,
          "should be method read in Instance and return correct data",
          "read",
          `I'm ${name}. I can read.`
        );

        makeTestRoleMethod(
          classInstance,
          "should be method write in Instance and return correct data",
          "write",
          `I'm ${name}. I can write.`
        );

        makeTestRoleMethod(
          classInstance,
          "should be method update in Instance and return correct data",
          "update",
          `I'm ${name}. I can update.`
        );

        it("shouldn't exist method remove", () => {
          expect("remove" in classInstance).toBeFalsy();
        });  

      });    
    });
  });

  describe("Client", () => {
    forEachMockData(data, Client, (classInstance, currentMockData) => {
      const { name, surname, email, yearOfBirth } = currentMockData;

      describe(`Client ${name} ${surname}`, () => {

        makeTestEmail(
          classInstance,
          "should be email in Instance",
          email
        );
        
        makeTestUserMethod(
          classInstance,
          "should be method getFullname in Instance and return correct data",
          "getFullname",
          `${name} ${surname}`
        );

        makeTestUserMethod(
          classInstance,
          "should be method getAge in Instance and return correct data",
          "getAge",
          new Date().getFullYear() - yearOfBirth
        );

        makeTestRoleMethod(
          classInstance,
          "should be method read in Instance and return correct data",
          "read",
          `I'm ${name}. I can read.`
        );

        makeTestRoleMethod(
          classInstance,
          "should be method write in Instance and return correct data",
          "write",
          `I'm ${name}. I can write.`
        );

        it("shouldn't exist methods update and remove", () => {
          expect("update" in classInstance).toBeFalsy();
          expect("remove" in classInstance).toBeFalsy();
        });
        
      });
    });
  });

  describe("Guest", () => {
    forEachMockData(data, Guest, (classInstance, currentMockData) => {
      const { name, surname, email, yearOfBirth } = currentMockData;

      describe(`Guest ${name} ${surname}`, () => {

        makeTestEmail(
          classInstance,
          "should be email in Instance",
          email
        );
        
        makeTestUserMethod(
          classInstance,
          "should be method getFullname in Instance and return correct data",
          "getFullname",
          `${name} ${surname}`
        );

        makeTestUserMethod(
          classInstance,
          "should be method getAge in Instance and return correct data",
          "getAge",
          new Date().getFullYear() - yearOfBirth
        );

        makeTestRoleMethod(
          classInstance,
          "should be method read in Instance and return correct data",
          "read",
          `I'm ${name}. I can read.`
        );

        it("shouldn't exist methods write, update and remove", () => {
          expect("write" in classInstance).toBeFalsy();
          expect("update" in classInstance).toBeFalsy();
          expect("remove" in classInstance).toBeFalsy();
        });
        
      });
    });
  });
});