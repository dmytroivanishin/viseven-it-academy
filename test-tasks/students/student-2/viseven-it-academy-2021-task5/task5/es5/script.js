/*export*/ const User = function() {

};

/*export*/ const Admin = function() {

};

/*export*/ const Moderator = function() {

};

/*export*/ const Client = function() {

};

/*export*/ const Guest = function() {

};

const admin = new Admin("Dmytro", "Yummy", "dmytro@test.com", 1995);
const moderator = new Moderator("Alex", "Morti", "alex@test.com", 1982);
const client = new Client("John", "Smith", "john@test.com", 1976);
const guest = new Guest("Robert", "Merlo", "robert@test.com", 1999);

admin.getAge();
admin.getFullname();
admin.read();
admin.write();
admin.update();
admin.remove();

moderator.getAge();
moderator.getFullname();
moderator.read();
moderator.write();
moderator.update();

client.getAge();
client.getFullname();
client.read();
client.write();

guest.getAge();
guest.getFullname();
guest.read();