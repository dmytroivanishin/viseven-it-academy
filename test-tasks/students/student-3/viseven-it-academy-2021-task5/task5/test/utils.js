export const forEachMockData = (mockData, className, fn) => {
  mockData.forEach(currentMockData => {
    const { name, surname, email, yearOfBirth } = currentMockData;

    const classInstance = new className(name, surname, email, yearOfBirth);

    fn(classInstance, currentMockData);
  });
};

export const removeSpesialChars = (str) => {
  return str.replace(/([^a-z]|Smith|Nikolson)/gi, "");
};

export const makeTestUserMethod = (classInstance, name, method, expectValue) => {
  it(name, () => {
    expect(method in classInstance).toBeTruthy();
    expect(classInstance[method]()).toBe(expectValue);
  });
};

export const makeTestRoleMethod = (classInstance, name, method, expectValue) => {
  it(name, () => {
    expect(method in classInstance).toBeTruthy();
    expect(removeSpesialChars(classInstance[method]())).toBe(removeSpesialChars(expectValue));
  });
};


export const makeTestEmail = (classInstance, name, expectValue) => {
  it(name, () => {
    expect(classInstance.hasOwnProperty("email")).toBeTruthy();
    expect(classInstance["email"]).toBe(expectValue);
  });
};