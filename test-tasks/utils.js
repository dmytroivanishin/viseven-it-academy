const fs = require("fs");
const path = require("path");
const util = require("util");

const destinationToFolderStudent = "viseven-it-academy-2021-task5/task5";

const destinationToTestTemplates = ["./templates/es6.js", "./templates/es5.js", "./templates/utils.js"];
const filenameTestFiles = ["es6.spec.js", "es5.spec.js", "utils.js"];

const readdirPromise = util.promisify(fs.readdir);
const readFilePromise = util.promisify(fs.readFile);
const writePromise = util.promisify(fs.appendFile);
const mkdir = util.promisify(fs.mkdir);

const getTemplates = async () => Promise.all(destinationToTestTemplates.map(t => readFilePromise(t)));

const getFolderToTest = (studentFolder) => {
  return path.resolve(
    __dirname,
    "students",
    studentFolder,
    destinationToFolderStudent,
    "test",
  );
};

const getPathToTest = (studentFolder, filename) => `${getFolderToTest(studentFolder)}/${filename}`;

module.exports = {
  destinationToFolderStudent,
  destinationToTestTemplates,
  filenameTestFiles,
  readdirPromise,
  readFilePromise,
  writePromise,
  mkdir,
  getTemplates,
  getFolderToTest,
  getPathToTest
}