module.exports = {
  transform: {
    "^.+\\.js$": "babel-jest"
  },
  transformIgnorePatterns: [
    "<rootDir>/node_modules/"
  ],
  coverageReporters: [
    "html"
  ],
  verbose: true,
  collectCoverageFrom: ["./students/**/*.js"],
  reporters: [
    "default",
    ["jest-html-reporters", {
      "publicPath": "./html-report"
    }]
  ]
}