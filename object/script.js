const user = {
  name: "Alex",
  age: 30
};

console.log(user)
console.log(user.name)
console.log(user["name"]);

delete user.age;

console.log(user);

const generateUser = (name, surname, age) => {
  return {
    name,
    surname,
    currentDate: 2021,
    showFullname() {
      alert( this.name + " " + this.surname );
    }
  };
};

const alex = generateUser("Alex", "Merlo", 30);
const john = generateUser("John", "Smith", 25);

console.log(alex);
console.log(john);

alex.showFullname();
john.showFullname();

const showFullnameAlex = alex.showFullname;

/*
const showFullnameAlex = function () {
  alert( this.name + " " + this.surname );
};

alert(showFullnameAlex)

window.name = "Merry";
window.surname = "Merry";

showFullnameAlex.call(john);

console.log( Object.entries(alex) );
*/

/*
const user = {
  id: 218646,
  name: "John",
  age: 30
};

const info = {
  role: "admin",
  permission: "all",
  age: 42
};

const info2 = {
  role: "client",
  permission: "all",
  age: 54
};

const admin = { ...user, ...info };

console.log(admin);

const { id, ...rest } = admin;

console.log(id);
console.log(rest);

*/